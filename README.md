# Thriller - Michael Jackson
**Is the best Album made ever**



## Best Album Because
1. Every song on here is an instant hit

2. Thriller helped spring the music video industry

3. Was a number one top hit album on the charts

## 


- The music video of “Thriller” played in a Westwood theater for one week in 1983 to qualify for an Oscar nomination

- The “Thriller” album has sold more than 104 million copies.

- “Thriller’s” phenomenal success led to a breaking down of traditional racial barriers on FM radio at the time. 

> "because this is thriller, thriller night"
- *best lyrics ever*.

![Simply Amazing](/thriller.jpg)


## Tracklist

_Other Albums and Track List size_

|  Album Name  | Number of Track |
|:-------------|:-----------------|
|  Wanna Be Startin' Somethin'  | 1 |
|  Baby Be Mine  | 2 |
|  The Girl is Mine  | 3 |
|  Thriller  | 4 |
|  Beat it  | 5 |
| Billie Jean  | 6 |
|  Human Nature  | 7 |
|  P.Y.T  | 8 |
|  The Lady In My Life  | 9 |




